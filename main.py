#!/usr/bin/python3.6
import pyperclip, json, time

from flask import Flask, request, abort

app = Flask(__name__, static_url_path='/static')

@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route('/api/typing', methods=['GET'])
def get_content():

    if type(request.args.get('content')) is str and len(request.args.get('content')) >= 1:
        content = request.args.get('content')
        pyperclip.copy(content)

        return json.dumps({ "message": "OK", "timestamp": time.time() })
    else:
        return abort(400)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5080')