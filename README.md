# Type from Web

## Introduction
This script allows you to type words from your phone, another computer or any device with a browser, and send it right into your computer clipboard.

## Pre-requisites
Check out the required system dependencies from Pyperclip:

[https://github.com/asweigart/pyperclip](https://github.com/asweigart/pyperclip)